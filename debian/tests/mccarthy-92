#!/bin/sh

set -e

# Run an example based on https://rise4fun.com/Boogie/McCarthy-91
# This example is adapted to be wrong: We should not be able to verify it

TMPDIR=$(mktemp -d)
trap "rm -rf $TMPDIR" EXIT
cd $TMPDIR

cat <<EOF > test.bpl
procedure F(n: int) returns (r: int)
  ensures 100 < n ==> r == n - 10;
  ensures n <= 100 ==> r == 92; // This would have to be 91 for the program to verify
{
  if (100 < n) {
    r := n - 10;
  } else {
    call r := F(n + 11);
    call r := F(r);
  }
}
EOF

cat <<EOF > expected
Boogie program verifier finished with 0 verified, 1 error
EOF

boogie test.bpl > boogie_output
tail -n1 boogie_output > result
diff result expected
